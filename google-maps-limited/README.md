# \<google-maps-limited\>

A limited use case of google maps with clickable markers. Great for consumers of all use cases and fraimeworks compatable with lit element.


## Contributing

### Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) and npm (packaged with [Node.js](https://nodejs.org)) installed. Run `npm install` to install your element's dependencies, then run `polymer serve` to serve your element locally. Right now the api key is hard coded for the demo. A pr is forth coming where you will have to eneter it in some way. For now please be kind with your demo usage.

### Viewing Your changes

```
$ polymer serve
```

### Running Tests

```
$ polymer test
```

